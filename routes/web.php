<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('card', function () {
    return view('card');
});

Route::get('card-tailwind', function () {
    return view('cardTailwind');
});

Route::get('flexbox-examples', function () {
    return view('flexboxExamples');
});

Route::get('sticky-footer', function () {
    return view('stickyFooter');
});

Route::get('faq', function () {
    return view('faq');
});

Route::get('pricing', function () {
    return view('pricing');
});

Route::get('banner', function () {
    return view('banner');
});

Route::get('responsive-demo', function () {
    return view('responsiveDemo');
});

Route::get('responsive-layout', function () {
    return view('responsiveLayout');
});

Route::get('rem', function () {
    return view('rem');
});

Route::get('modal', function () {
    return view('modal');
});
