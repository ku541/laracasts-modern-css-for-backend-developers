<!DOCTYPE html>
<html lang="en" class="text-xs lg:text-base">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Modal</title>

        {{-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> --}}
        <style>
            .cancel {
                height: 100%;
                position: absolute;
                width: 100%;
            }

            .modal {
                background: white;
                border-radius: 8px;
                max-width: 80%;
                padding: 1em 2em;
                position: relative;
                width: 600px;
            }

            .modal .close {
                color: grey;
                position: absolute;
                right: 15px;
                text-decoration: none;
                top: 15px;
            }

            .overlay {
                align-items: center;
                background: rgba(0, 0, 0, .7);
                bottom: 0;
                display: flex;
                justify-content: center;
                left: 0;
                position: absolute;
                right: 0;
                top: 0;
                visibility: hidden;
            }

            .overlay:target {
                visibility: visible;
            }
        </style>
    </head>

    <body>
        <li>
            <a href="#join-modal">Join</a>
        </li>

        <li>
            <a href="#cancel-modal">Cancel</a>
        </li>

        @component('components.modal', ['name' => 'join-modal'])
            <h1>Pick a Plan</h1>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Veritatis amet, architecto expedita aperiam ullam eaque
                magnam doloribus sint nulla esse molestias perspiciatis
                soluta quis illum commodi atque, delectus aspernatur!
                Quam?</p>
        @endcomponent

        @component('components.modal', ['name' => 'cancel-modal'])
            <h1>Leaving So Soon?</h1>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Veritatis amet, architecto expedita aperiam ullam eaque
                magnam doloribus sint nulla esse molestias perspiciatis
                soluta quis illum commodi atque, delectus aspernatur!
                Quam?</p>

            <p><a href="#join-modal">Sign up</a></p>
        @endcomponent
    </body>
</html>
