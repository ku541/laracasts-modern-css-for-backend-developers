<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Card</title>

        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/card.css">
    </head>

    <body>
        <div class="card">
            {{-- left --}}
            <div class="card-left">
                <a href="#" class="card-skill-button">PHP</a>

                <img src="https://laracasts.s3.amazonaws.com/series/thumbnails/testing-jargon.png">

                <span class="card-difficulty">Intermediate Difficulty</span>
            </div>

            {{-- right --}}
            <div class="card-right">
                <h3 class="card-title"><a href="#">Testing Jargon</a></h3>

                <p class="card-excerpt">There's no two ways about it: terminology in the testing world is incredibly
                    overwhelming. Let's fix that! Bit by bit, we'll break all of these confusing concepts down as best
                    as we can.</p>

                <div class="card-meta">
                    <div>
                        <i class="fa fa-book" aria-hidden="true"></i>
                        8 episodes
                    </div>

                    <div>
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        1h 5m
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
