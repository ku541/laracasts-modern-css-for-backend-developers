<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>CSS for Backend Devs</title>

        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/header.css">
    </head>

    <body>
        <header class="section">
            <div class="container">
                <div class="header-top">
                    <h1>XCasts</h1>

                    <a href="#">Sign In</a>
                </div>

                <nav>
                    <a href="#">Catalog</a>

                    <a href="#">Series</a>

                    <a href="#">Podcast</a>

                    <a href="#">Discussions</a>
                </nav>
            </div>
        </header>

        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="box">
                            <p>Some Callout Text</p>
                            <p>Some Callout Text</p>
                            <p>Some Callout Text</p>
                        </div>
                    </div>

                    <div class="col">
                        <div class="box">
                            Some Callout Text
                        </div>
                    </div>

                    <div class="col">
                        <div class="box">
                            Some Callout Text
                        </div>
                    </div>

                    <div class="col">
                        <div class="box">
                            Some Callout Text
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
