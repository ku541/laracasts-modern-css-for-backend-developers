<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Sticky Footer</title>

        <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    </head>

    <body class="container mx-auto">
        <div class="border-b py-8">
            <h1 class="font-bold mb-6 text-gray-600 text-sm tracking-wide uppercase">Instant Navigation</h1>

            <nav class="bg-blue-100 flex justify-between p-4">
                <a href="#">Home</a>
                <a href="#">About</a>
                <a href="#">Our Mission</a>
                <a href="#">Contact</a>
            </nav>
        </div>

        <div class="border-b py-8">
            <h1 class="font-bold mb-6 text-gray-600 text-sm tracking-wide uppercase">Split Nav</h1>

            <nav class="bg-blue-100 flex justify-between p-4">
                <div>
                    <a href="#" class="px-4">Home</a>
                    <a href="#" class="px-4">About</a>
                </div>

                <div>
                    <a href="#" class="px-4">Our Mission</a>
                    <a href="#" class="px-4">Contact</a>
                </div>
            </nav>
        </div>

        <div class="border-b py-8">
            <h1 class="font-bold mb-6 text-gray-600 text-sm tracking-wide uppercase">Align Image With Text</h1>

            <div class="flex items-center">
                <img src="http://via.placeholder.com/350x150" class="mr-4">

                <div>
                    <h3 class="mb-4">My Trip To...</h3>

                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur consectetur minus atque
                        ipsam repellat architecto, rem quis corporis, esse quo a dolorum natus voluptate similique quam
                        dicta cupiditate asperiores suscipit.</p>
                </div>
            </div>
        </div>

        <div class="border-b py-8">
            <h1 class="font-bold mb-6 text-gray-600 text-sm tracking-wide uppercase">Perfectly Centered Text</h1>

            <div class="bg-gray-400 flex h-64 items-center justify-center p-6 w-3/4">
                <div class="w-64">
                    <h3 class="font-bold mb-4">Flexbox is Amazing</h3>

                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Lorem ipsum dolor sit amet consectetur
                        adipisicing elit.</p>
                </div>
            </div>
        </div>

        <div class="border-b py-8">
            <h1 class="font-bold mb-6 text-gray-600 text-sm tracking-wide uppercase">Sticky Footer</h1>

            <a href="/sticky-footer">Click Me</a>
        </div>
    </body>
</html>
