<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Responsive Demo</title>

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <style>
            .flex-2 {
                flex: 8;
            }
        </style>
    </head>

    <body>
        <div class="md:flex md:flex-wrap">
            <div class="flex-1 p-4">
                <div class="bg-gray-400 p-4">1</div>
            </div>

            <div class="flex-2 p-4">
                <div class="bg-gray-400 p-4">2</div>
            </div>

            <div class="flex-1 p-4">
                <div class="bg-gray-400 p-4">3</div>
            </div>

            <div class="flex-1 lg:mr-6 p-4">
                <div class="bg-gray-400 p-4">4</div>
            </div>

            <div class="flex-1 p-4">
                <div class="bg-gray-400 p-4">5</div>
            </div>

            <div class="flex-1 p-4">
                <div class="bg-gray-400 p-4">6</div>
            </div>

            <div class="flex-1 p-4">
                <div class="bg-gray-400 p-4">7</div>
            </div>

            <div class="flex-1 p-4">
                <div class="bg-gray-400 p-4">8</div>
            </div>
        </div>
    </body>
</html>
