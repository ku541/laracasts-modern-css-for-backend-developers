<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Pricing</title>

        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/css/pricing.css">
    </head>

    <body class="font-sans p-6">
        <div class="container mx-auto">
            <div class="flex">
                <div class="flex flex-col justify-around mx-4 p-6 shadow text-center w-1/4">
                    <div class="flex justify-between mb-2">
                        <h5 class="font-bold text-gray-600 tracking-tight uppercase">Monthly</h5>

                        <a href="#" class="flex font-bold items-baseline leading-none text-blue-500">
                            <span class="text-lg">$</span>
                            <span class="text-4xl">15</span>
                        </a>
                    </div>

                    <div class="flex justify-center">
                        <img src="https://laracasts.com/images/plans/sub-monthly.svg?v=3" alt="Monthly Plan"
                            class="w-40">
                    </div>

                    <p class="leading-normal mb-8 mt-2 py-4 text-gray-700">Be in the know. Fetch an endless stream of
                        input with our monthly plan.</p>

                    <a href="#" class="bg-blue-500 font-semibold px-6 py-3 rounded-full text-white text-xs uppercase">
                        Start Learning</a>
                </div>

                <div class="flex flex-col justify-around mx-4 p-6 shadow text-center w-1/4">
                    <div class="flex justify-between mb-2">
                        <h5 class="font-bold text-gray-600 tracking-tight uppercase">Monthly</h5>

                        <a href="#" class="flex font-bold items-baseline leading-none text-blue-500">
                            <span class="text-lg">$</span>
                            <span class="text-4xl">15</span>
                        </a>
                    </div>

                    <div class="flex justify-center">
                        <img src="https://laracasts.com/images/plans/sub-monthly.svg?v=3" alt="Monthly Plan"
                            class="w-40">
                    </div>

                    <p class="leading-normal mb-8 mt-2 py-4 text-gray-700">Be in the know. Fetch an endless stream of
                        input with our monthly plan.</p>

                    <a href="#" class="bg-blue-500 font-semibold px-6 py-3 rounded-full text-white text-xs uppercase">
                        Start Learning</a>
                </div>

                <div class="flex flex-col justify-around mx-4 p-6 shadow text-center w-1/4">
                    <div class="flex justify-between mb-2">
                        <h5 class="font-bold text-gray-600 tracking-tight uppercase">Monthly</h5>

                        <a href="#" class="flex font-bold items-baseline leading-none text-blue-500">
                            <span class="text-lg">$</span>
                            <span class="text-4xl">15</span>
                        </a>
                    </div>

                    <div class="flex justify-center">
                        <img src="https://laracasts.com/images/plans/sub-monthly.svg?v=3" alt="Monthly Plan"
                            class="w-40">
                    </div>

                    <p class="leading-normal mb-8 mt-2 py-4 text-gray-700">Be in the know. Fetch an endless stream of
                        input with our monthly plan.</p>

                    <a href="#" class="bg-blue-500 font-semibold px-6 py-3 rounded-full text-white text-xs uppercase">
                        Start Learning</a>
                </div>

                <div class="flex flex-col justify-around mx-4 p-6 shadow text-center w-1/4">
                    <div class="flex justify-between mb-2">
                        <h5 class="font-bold text-gray-600 tracking-tight uppercase">Monthly</h5>

                        <a href="#" class="flex font-bold items-baseline leading-none text-blue-500">
                            <span class="text-lg">$</span>
                            <span class="text-4xl">15</span>
                        </a>
                    </div>

                    <div class="flex justify-center">
                        <img src="https://laracasts.com/images/plans/sub-monthly.svg?v=3" alt="Monthly Plan"
                            class="w-40">
                    </div>

                    <p class="leading-normal mb-8 mt-2 py-4 text-gray-700">Be in the know. Fetch an endless stream of
                        input with our monthly plan.</p>

                    <a href="#" class="bg-blue-500 font-semibold px-6 py-3 rounded-full text-white text-xs uppercase">
                        Start Learning</a>
                </div>
            </div>
        </div>
    </body>
</html>
