<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Responsive Demo 2</title>

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    </head>

    <body>
        <div class="container mx-auto">
            <div class="md:flex md:flex-col md:min-h-screen">
                <header class="bg-red-400 p-3">
                    <h1>My Site</h1>
                </header>

                <div class="md:flex md:flex-1">
                    <aside class="bg-green-400 p-3">Sidebar</aside>

                    <main class="bg-blue-400 md:flex-1 p-3">
                        <div class="flex flex-wrap">
                            <div class="p-3 md:w-1/4 w-1/2">
                                <div class="bg-gray-400 md:mb-2 p-3">
                                    <p>Product Feature</p>
                                </div>
                            </div>

                            <div class="p-3 md:w-1/4 sm:w-1/2">
                                <div class="bg-gray-400 md:mb-2 p-3">
                                    <p>Product Feature</p>
                                </div>
                            </div>

                            <div class="p-3 md:w-1/4 sm:w-1/2">
                                <div class="bg-gray-400 md:mb-2 p-3">
                                    <p>Product Feature</p>
                                </div>
                            </div>

                            <div class="p-3 md:w-1/4 sm:w-1/2">
                                <div class="bg-gray-400 md:mb-2 p-3">
                                    <p>Product Feature</p>
                                </div>
                            </div>

                            <div class="p-3 md:w-1/4 sm:w-1/2">
                                <div class="bg-gray-400 md:mb-2 p-3">
                                    <p>Product Feature</p>
                                </div>
                            </div>

                            <div class="p-3 md:w-1/4 sm:w-1/2">
                                <div class="bg-gray-400 md:mb-2 p-3">
                                    <p>Product Feature</p>
                                </div>
                            </div>

                            <div class="p-3 md:w-1/4 sm:w-1/2">
                                <div class="bg-gray-400 md:mb-2 p-3">
                                    <p>Product Feature</p>
                                </div>
                            </div>

                            <div class="p-3 md:w-1/4 sm:w-1/2">
                                <div class="bg-gray-400 md:mb-2 p-3">
                                    <p>Product Feature</p>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>

                <footer class="bg-yellow-400 p-3">
                    Copyright {{ now()->year }}
                </footer>
            </div>
        </div>
    </body>
</html>
