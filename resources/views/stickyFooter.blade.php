<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Sticky Footer</title>

        <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    </head>

    <body class="container mx-auto">
        <div id="root" class="flex flex-col min-h-screen">
            <header class="bg-blue-100 mb-6 p-8">
                <h1>My Website</h1>
            </header>

            <main class="flex-1 w-3/4">
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit architecto quasi consequuntur fuga
                    at, est quod dolore exercitationem rem delectus tempora similique illum temporibus ipsam sit cumque
                    recusandae suscipit aspernatur?</p>

                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit architecto quasi consequuntur fuga
                    at, est quod dolore exercitationem rem delectus tempora similique illum temporibus ipsam sit cumque
                    recusandae suscipit aspernatur?</p>

                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit architecto quasi consequuntur fuga
                    at, est quod dolore exercitationem rem delectus tempora similique illum temporibus ipsam sit cumque
                    recusandae suscipit aspernatur?</p>

                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit architecto quasi consequuntur fuga
                    at, est quod dolore exercitationem rem delectus tempora similique illum temporibus ipsam sit cumque
                    recusandae suscipit aspernatur?</p>
            </main>

            <footer class="bg-blue-100 py-8">
                Laracasts - 2020
            </footer>
        </div>
    </body>
</html>
