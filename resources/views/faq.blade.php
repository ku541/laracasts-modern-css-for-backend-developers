<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>FAQ</title>

        <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/header.css">
        <link rel="stylesheet" href="/css/faq.css">

        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    </head>

    <body>
        <header class="section">
            <div class="container">
                <div class="header-top">
                    <h1>XCasts</h1>

                    <a href="#">Sign In</a>
                </div>

                <nav>
                    <a href="#">Catalog</a>

                    <a href="#">Series</a>

                    <a href="#">Podcast</a>

                    <a href="#">Discussions</a>
                </nav>
            </div>
        </header>

        <div class="section">
            <div class="container">
                <div class="generic-content m-auto w-3/5">
                    <div class="mb-10 text-center text-gray-700">
                        <h1 class="mb-1 text-3xl">FAQ</h1>

                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </div>

                    <div class="flex mb-8" x-data="{ open: false }">
                        <div class="cursor-pointer mr-4">
                            <i class="border border-gray-700 fa fa-plus flex h-6 items-center justify-center rounded-full text-gray-700 w-6"
                                aria-hidden="true" x-show="! open" @click="open = true"></i>

                            <i class="border border-gray-700 fa fa-minus flex h-6 items-center justify-center rounded-full text-gray-700 w-6"
                                aria-hidden="true" x-show="open" @click="open = false"></i>
                        </div>

                        <div class="leading-relaxed">
                            <h3 class="cursor-pointer font-bold mb-3 leading-none text-2xl" @click="open = ! open">Similique fugiat
                                accusantium et voluptates iure?</h3>

                            <div class="text-gray-700" x-show="open">Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit. Minima nam, magni doloribus hic consequatur excepturi voluptate
                                provident ab alias, ea iure eligendi ipsum facere aliquam eum laborum illo quaerat
                                itaque.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
