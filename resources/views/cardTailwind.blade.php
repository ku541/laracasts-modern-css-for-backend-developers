<!DOCTYPE html>
<html lang="en" class="h-full">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Card Tailwind</title>

        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/card-tailwind.css">
    </head>

    <body class="flex h-full items-center justify-center">
        <div class="card flex rounded">
            {{-- left --}}
            <div class="card-left flex flex-col items-center justify-between p-6 rounded text-center text-white">
                <a href="#" class="bg-black card-skill-button opacity-50 px-6 py-1 rounded-full text-xs">PHP</a>

                <img class="max-w-full" src="https://laracasts.s3.amazonaws.com/series/thumbnails/testing-jargon.png">

                <span class="card-difficulty text-xs">Intermediate Difficulty</span>
            </div>

            {{-- right --}}
            <div class="card-right flex flex-col p-6 text-gray-600">
                <h3 class="card-title font-light mb-2 rounded-full text-gray-900 text-2xl">
                    <a href="#">Testing Jargon</a>
                </h3>

                <p class="card-excerpt flex-1 leading-snug text-sm">There's no two ways about it: terminology in the
                    testing world is incredibly overwhelming. Let's fix that! Bit by bit, we'll break all of these
                    confusing concepts down as best as we can.</p>

                <div class="card-meta flex text-xs">
                    <div class="flex items-center mr-4">
                        <i class="fa fa-book mr-2" aria-hidden="true"></i>
                        8 episodes
                    </div>

                    <div class="flex items-center">
                        <i class="fa fa-clock-o mr-2" aria-hidden="true"></i>
                        1h 5m
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
