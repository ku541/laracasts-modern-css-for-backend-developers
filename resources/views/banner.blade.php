<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Banner</title>

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="{{ mix('css/banner.css') }}">
    </head>

    <body>
        <section class="py-0 signup-banner text-white">
            <div class="container mx-auto">
                <div class="flex h-screen items-center justify-center">
                    <div class="mr-9">
                        <img src="https://laracasts.com/images/call-to-action/robot-illustration.svg?v=2"
                            alt="Laracasts Sign Up Mascot">
                    </div>

                    <div class="w-1/2">
                        <h3 class="mb-6 text-3xl">The most concise screencasts for the working developer, updated
                            daily.</h3>

                        <p class="mb-10">There's no shortage of content at Laracasts. In fact, you could watch nonstop
                            for days upon days, and still not see everything!</p>

                        <p><a href="#"
                                class="border border-white font-semibold hover:bg-white hover:border-white hover:text-blue-500 mt-10 px-9 py-3 rounded-full text-xs uppercase">Get
                                Started</a></p>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
