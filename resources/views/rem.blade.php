<!DOCTYPE html>
<html lang="en" class="text-xs lg:text-base">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Rem</title>

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <style>
            .section {
                padding-left: 20px;
                padding-right: 20px;
            }

            .content>* {
                margin-bottom: 1em;
            }
        </style>
    </head>

    <body>
        <div class="section">
            <div class="container mx-auto">
                <header>
                    <h1 class="mb-8 text-3xl">Laracasts</h1>
                </header>

                <main class="content">
                    <h2>About Us</h2>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam facilis sequi perspiciatis
                        perferendis, aperiam, unde impedit rerum error soluta ex, sunt ut nisi quos ratione voluptas
                        in molestias odit repudiandae. Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum
                        ducimus suscipit minus vel laboriosam, possimus quas et, cum voluptatum sapiente reprehenderit.
                        Ut porro nobis eveniet temporibus corrupti, fuga enim similique?</p>

                    <h2>Our Mission</h2>

                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quis quo vitae modi voluptas adipisci
                        maiores ipsam iusto dignissimos molestias sapiente temporibus omnis fuga, saepe nulla
                        repudiandae cumque architecto facere laborum? Lorem ipsum dolor sit amet consectetur,
                        adipisicing elit. Ullam exercitationem vel incidunt nihil provident officiis accusamus
                        temporibus impedit, reiciendis perspiciatis quod sit facilis ducimus! Quisquam aliquid
                        distinctio similique iusto culpa?</p>

                    <h3>How We Get There...</h3>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam facilis sequi perspiciatis
                        perferendis, aperiam, unde impedit rerum error soluta ex, sunt ut nisi quos ratione voluptas
                        in molestias odit repudiandae. Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum
                        ducimus suscipit minus vel laboriosam, possimus quas et, cum voluptatum sapiente reprehenderit.
                        Ut porro nobis eveniet temporibus corrupti, fuga enim similique?</p>

                    <h2>Our Mission</h2>

                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quis quo vitae modi voluptas adipisci
                        maiores ipsam iusto dignissimos molestias sapiente temporibus omnis fuga, saepe nulla
                        repudiandae cumque architecto facere laborum? Lorem ipsum dolor sit amet consectetur,
                        adipisicing elit. Ullam exercitationem vel incidunt nihil provident officiis accusamus
                        temporibus impedit, reiciendis perspiciatis quod sit facilis ducimus! Quisquam aliquid
                        distinctio similique iusto culpa?</p>
                </main>
            </div>
        </div>
    </body>
</html>
